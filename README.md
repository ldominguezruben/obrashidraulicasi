# Post-Processing

En este repo irán los códigos utilizados para procesar los archivos de manera de plotear los datos necesarios para visualizar variables

## Generalidades

- Cada script de procesamiento debe indicar el archivo de entrada que utiliza y el archivo de salida que produce
- Cada carpeta debe tener dentro un README que indique qué contiene cada archivo ahí presente

### Archivos de salida

- Los archivos de salida son CSV o TXT, pero los separadores de columnas es convenientes que sean ";" (punto y coma)
- Por cuestiones de visualización se recomienda también usar TABs, pero obligadamente usar el separador ";"
- Cada archivo de salida debe tener un encabezado que indique qué es cada columna
- Los números flotantes usan "." punto como serador decimal
- 

### Scripts

Cada script deberá tener tres partes:
- Una versión amigable, acotada y con espíritu docente se encuentre en un notebook jupyter, ya veremos si usamos colab o algo mas libre, quedará por definir, ver: https://www.dataschool.io/cloud-services-for-jupyter-notebook/
- El script propiamente dicho

### Estructura

- El repo cuenta con 3 carpetas 
-- La carpeta web donde se aloja todos los recursos para la web
-- La carpeta 'dataset' donde se alojan los datos 
-- La carpeta 'graph' dopnde se alojan los graficos que se van generando  
